# This image holds the runtime dependencies required to run the training
# program. It does not however contain the program itself.
FROM ubuntu:bionic

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV SPARK_HOME=/opt/spark
ENV REPO="git@bitbucket.org:chooseapp/ml-training.git"
ENV COORDINATOR_ENDPOINT="http://coordinator.ml.appchoose.local:8000/jobs"

RUN apt-get update
RUN apt-get install -y curl git gnupg openjdk-8-jdk-headless scala liblapack3 libblas3 libgfortran3 libsnappy-jni python3-venv python3-setuptools software-properties-common vim \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# Build and install spark (without hadoop)
WORKDIR /workdir
RUN git clone --depth=1 -b v2.4.0 https://github.com/apache/spark.git
RUN cd spark \
    && ./dev/make-distribution.sh --name appchoose --pip --tgz -Phadoop-provided -Phive -Phive-thriftserver -Pyarn -Pnetlib-lgpl \
    && tar -C /opt -zxvf spark-2.4.0-bin-appchoose.tgz \
    && ln -s /opt/spark-* /opt/spark \
    && rm -rf /workdir/spark

# Configure spark
ADD assets/spark-env.sh /opt/spark/conf/spark-env.sh

# Install Hadoop
RUN cd /workdir && \
    curl -O http://apache.mirrors.ovh.net/ftp.apache.org/dist/hadoop/common/hadoop-3.2.0/hadoop-3.2.0.tar.gz \
    && tar -C /opt -zxf hadoop-*.tar.gz && rm hadoop-*.tar.gz \
    && ln -s /opt/hadoop-* /opt/hadoop \
    && ln -s /opt/hadoop/share/hadoop/tools/lib/aws-java-sdk-*.jar /opt/hadoop/share/hadoop/common/lib/ \
    && ln -s /opt/hadoop/share/hadoop/tools/lib/hadoop-aws-*.jar /opt/hadoop/share/hadoop/common/lib/

# Install python libs
RUN cd /opt/spark/python \
    && python setup.py sdist \
    && python -m venv /opt/venv

ADD requirements.txt /workdir/
RUN . /opt/venv/bin/activate \
    && pip install /opt/spark/python/dist/* \
    && pip install -r requirements.txt

# Add ssh keys, and config
ADD assets/ssh/* /root/.ssh/

# Add entrypoint script
ADD assets/entrypoint.sh /workdir/

ENTRYPOINT ["/workdir/entrypoint.sh"]
