# AppChoose Machine Learning Training Service

This module implements a recommender system using the Spark Python API.

## Getting Started

### Download Apache Spark 2.4.0

```shell
$ cd ~
$ wget http://apache.crihan.fr/dist/spark/spark-2.4.0/spark-2.4.0-bin-hadoop2.7.tgz
$ tar -zxvf spark-2.4.0-bin-hadoop2.7.tgz
```

### Clone the repository

```shell
$ git clone git@bitbucket.org:chooseapp/ml-training.git
```

Edit the `config-example.json` file accordingly and save it as `config.json`. 

### Create a virtual environment

```shell
$ virtualenv --python=python3.6 venv-ml-training
$ source venv-ml/bin/activate
```

Install the module in development mode together with required dependencies. The
`-e` flag installs the ml-training package in development mode, so that changes made to
the source code are reflected immedately in the installed package (there is no
need to re-install to see new changes).

```shell
$ pip install -e ml-training
```

Train the model in development mode

```shell
$ cd ml-training
$ python -m ml_training.app
```

## Troubleshooting

### Missing classes

As we load parquet files from s3, we need to make sure that both `hadoop-aws-2.7.3.jar` and `aws-java-sdk-1.7.4.jar` are not missing. In case they are, proceed as follows:

```shell
$ cd ~/spark-2.4.0-bin-hadoop2.7/jars
# hadoop-aws-2.7.3.jar
$ wget http://central.maven.org/maven2/org/apache/hadoop/hadoop-aws/2.7.3/hadoop-aws-2.7.3.jar
# aws-java-sdk-1.7.4.jar
$ wget http://central.maven.org/maven2/com/amazonaws/aws-java-sdk/1.7.4/aws-java-sdk-1.7.4.jar
```

