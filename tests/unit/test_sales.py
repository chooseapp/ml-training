import pytest
from ml_training.sales import model
import json
import numpy as np

with open('config-dev.json', 'r') as f:
    config = json.load(f)

@pytest.fixture
def fake_model():
    return(model.SparkRecommenderSystem(config))

def test_recommendation_model_initialization(fake_model):
    assert isinstance(fake_model.model, dict)

def test_get_sale_id(fake_model):
    assert fake_model.get_sale_id('{"saleId": "3reXOe75"}') == "3reXOe75" 

def test_train_model(fake_model):
    #m = fake_model.train_model(condition = "event_name = 'AddToCart' AND event_time BETWEEN 1549032794 AND 1549291994")
    #assert isinstance(fake_model.model['sale_factors'], np.ndarray)
    #assert isinstance(fake_model.model['user_factors'], np.ndarray)
    #assert fake_model.model['sale_factors'].shape[0] == len(fake_model.model['sale_mapping']['by_sale'])
    #assert fake_model.model['user_factors'].shape[0] == len(fake_model.model['user_mapping']['by_user'])
    #assert fake_model.model['sale_factors'].shape[1] == fake_model.model['user_factors'].shape[1]
    pass