import psycopg2
import pandas as pd
import pickle
import boto3
import numpy as np
import datetime
import json
from sklearn.utils import resample
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

class GenderModel():

    def __init__(self, config):
        self.aws_bucket_analytics = config['AWS']['AWS_BUCKET_NAME']['ANALYTICS']
        self.aws_bucket_models = config['AWS']['AWS_BUCKET_NAME']['MODELS']

        self.aws_access_key_id = config['AWS'].get('AWS_ACCESS_KEY_ID')
        self.aws_secret_access_key = config['AWS'].get('AWS_SECRET_ACCESS_KEY')
        
        self.model=self.init_model()
        self.generate_dataset(config)
        
    @staticmethod
    def init_model():
        return {
            'app_mapping': {
                'by_app': None
            },
            'clf': None
        }

    @staticmethod
    def create_blacklist(table, min_count=5):
        counts = table.groupby('first_name').count()
        return list(counts[counts['user_id'] < min_count].index.values)

    def load_gender_table(self, config, min_count=5):
    
        conn = psycopg2.connect(
            host=config['GENDER_DB']['HOST'], 
            port=config['GENDER_DB']['PORT'],
            database=config['GENDER_DB']['DATABASE'], 
            user=config['GENDER_DB']['USER'], 
            password=config['GENDER_DB']['PASSWORD']
        )
    
        table = pd.io.sql.read_sql_query('''SELECT * FROM gender''', conn)
    
        conn.close()

        blacklist = self.create_blacklist(table[['first_name', 'user_id']], min_count=min_count)
        blacklist = blacklist + ['CAMILLE', 'DOMINIQUE']

        # remove blacklisted entries
        table = table[~table['first_name'].isin(blacklist)]
    
        return table

    def load_user_apps(self, config, user_ids):

        conn = psycopg2.connect(
            host=config['POSTGRESQL']['HOST'], 
            database=config['POSTGRESQL']['DATABASE'], 
            user=config['POSTGRESQL']['USER'], 
            password=config['POSTGRESQL']['PASSWORD']
        )

        table = pd.io.sql.read_sql_query('''
            SELECT user_key as user_id, apps 
            FROM users
            WHERE user_key IN ({}) AND apps IS NOT NULL''' \
                .format(','.join(list("'" + user_ids + "'"))), conn)

        conn.close()

        apps = pd.io.json.json_normalize(table['apps']).astype('float')
        apps['user_id'] = table['user_id']
        
        return apps

    def downsample(dataset):
        minority = dataset[dataset['gender'] == 'female']
        majority = dataset[dataset['gender']]

    def generate_dataset(self, config, random_state=0, balance_classes=True):
        
        gender_table=self.load_gender_table(config, min_count=5)
        user_table=self.load_user_apps(config, gender_table['user_id'])
        
        dataset = pd.merge(user_table, gender_table[['user_id', 'gender']], on='user_id')
        dataset = dataset.set_index('user_id')
        dataset = dataset.drop(['adopteunmec', 'amazon', 'badoo', 'bnp', 'choose', 'chrome', 'hq', 
                                'labanquepostale', 'pokemon', 'pullandbear', 'sephora', 'showroom', 
                                'urbanoutfitters'], axis=1).dropna(how='any')
        
        if balance_classes:
            # Separate majority and minority classes
            df_majority = dataset[dataset['gender']=='female']
            df_minority = dataset[dataset['gender']=='male']

            # Downsample majority class
            df_majority_downsampled = resample(
                df_majority, 
                replace=False,
                n_samples=df_minority.shape[0],
                random_state=0
            )
            
            print(df_majority_downsampled.shape)
            print(df_minority.shape)
            dataset = pd.concat([df_majority_downsampled, df_minority])
        
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(dataset.drop('gender', axis=1),
                                                                                dataset['gender'], 
                                                                                test_size=0.2, 
                                                                                random_state=random_state)

    def fit(self):

        self.model['app_mapping']['by_app'] = {
            k: v for v, k in enumerate(list(self.X_train.columns.values))
        }
        
        self.model['clf'] = LogisticRegression(solver='liblinear', 
                                               random_state=0, 
                                               C=0.5, 
                                               penalty='l2', 
                                               tol=1e-6, 
                                               max_iter=1000)
        
        self.model['clf'].fit(self.X_train, self.y_train)
        
        y_pred = self.model['clf'].predict(self.X_test)
        print('Accuracy: {}'.format(np.mean(self.y_test == y_pred)))
        from sklearn.metrics import confusion_matrix
        confusion_matrix = confusion_matrix(self.y_test, y_pred)
        print(confusion_matrix)
        
    def upload_model(self):
        date = datetime.datetime.now().strftime("%Y-%m-%d")
        model_path = 's3a://{}/sales/specialnew/{}'.format(self.aws_bucket_models, date)

        print("Saving the sklearn model into pickled files...")
        
        client = boto3.client(
            's3',
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key
        )

        client.put_object(
            Body=pickle.dumps(self.model), 
            Bucket=self.aws_bucket_models, 
            Key='gender/{}/model.pickle'.format(date)
        )     

        client.put_object(
            Body=json.dumps({'version': date}), 
            Bucket=self.aws_bucket_models, 
            Key='gender/version.json'.format(date)
        )   