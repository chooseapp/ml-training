import json
import os
import datetime
import boto3
import pickle
import numpy as np
import pandas as pd
from psycopg2 import connect
from ml_training.utils import invert_mapping
from pyspark.sql import Row

class SparkRecommenderSystem():
    
    def __init__(self, config):
        self.aws_bucket_analytics = config['AWS']['AWS_BUCKET_NAME']['ANALYTICS']
        self.aws_bucket_models = config['AWS']['AWS_BUCKET_NAME']['MODELS']

        self.aws_access_key_id = config['AWS'].get('AWS_ACCESS_KEY_ID')
        self.aws_secret_access_key = config['AWS'].get('AWS_SECRET_ACCESS_KEY')

        self.host = config['POSTGRESQL']['HOST']
        self.database = config['POSTGRESQL']['DATABASE']
        self.user = config['POSTGRESQL']['USER']
        self.password = config['POSTGRESQL']['PASSWORD'] 

        self.model = self.init_model()

    @staticmethod
    def init_model():
        return {
            'user_factors': {
                'latent': None,
                'apps': None
            },
            'user_mapping': {
                'by_index': None,
                'by_user': None
            },
            'sale_factors': {
                'latent': None
            },
            'sale_mapping': {
                'by_index': None
            },
            'app_mapping': {
                'by_index': None,
                'by_app': None
            }
        }

    def create_spark_session(self):
        from pyspark.sql import SparkSession
        session = SparkSession.builder.getOrCreate()

        # This is for local development only. The credentials will be fetched
        # from the MD server when running on AWS, and they should not be
        # explicitly passed.
        if self.aws_access_key_id is not None:
            session.sparkContext \
                    ._jsc.hadoopConfiguration() \
                    .set("fs.s3a.access.key", self.aws_access_key_id)
            session.sparkContext \
                    ._jsc.hadoopConfiguration() \
                    .set("fs.s3a.secret.key", self.aws_secret_access_key)
        return session

    @staticmethod
    def get_sale_id(event_properties: str) -> str:
        try:
            return json.loads(event_properties)['saleId']
        except:
            return ''

    def create_user_mapping(self, spark_df):
        self.model['user_mapping']['by_index'] = spark_df.select('user_id') \
            .distinct() \
            .toPandas()['user_id'] \
            .to_dict()
            
        self.model['user_mapping']['by_user'] = invert_mapping(self.model['user_mapping']['by_index'])
    
    def create_sale_mapping(self, spark_df):
        from pyspark.sql import Row
        self.model['sale_mapping']['by_index'] = spark_df.select('event_properties') \
            .rdd \
            .map(lambda r: Row(sale_id=self.get_sale_id(r[0]))) \
            .toDF() \
            .distinct() \
            .toPandas()['sale_id'] \
            .to_dict()
        
        self.model['sale_mapping']['by_sale'] = invert_mapping(self.model['sale_mapping']['by_index'])
        
    def update_user_mapping(self, userFactors):
        self.model['user_mapping']['by_index'] = userFactors['id'] \
            .apply(lambda x : self.model['user_mapping']['by_index'][x]) \
            .to_dict()
        self.model['user_mapping']['by_user'] = invert_mapping(self.model['user_mapping']['by_index'])
        
    def update_sale_mapping(self, saleFactors):
        self.model['sale_mapping']['by_index'] = saleFactors['id'] \
            .apply(lambda x : self.model['sale_mapping']['by_index'][x]) \
            .to_dict()
            
    def train_model(self, condition="event_name = 'AddToCart'"):
        from pyspark.sql import Row
        from pyspark.ml.recommendation import ALS
        from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
        from pyspark.ml.evaluation import RegressionEvaluator

        spark = self.create_spark_session()
        df = spark.read.parquet('s3a://{}/'.format(self.aws_bucket_analytics)).where(condition)
            
        self.create_user_mapping(df)
        self.create_sale_mapping(df)
        
        self.ratings = df.select('event_name', 'event_properties', 'user_id') \
                        .rdd \
                        .map(lambda r: Row(user_id=self.model['user_mapping']['by_user'][r[2]],                                            
                                           sale_id=self.model['sale_mapping']['by_sale'][self.get_sale_id(r[1])],
                                           event=r[0])) \
                        .toDF() \
                        .groupby(['user_id', 'sale_id']) \
                        .count()
        
        (self.training, self.test) = self.ratings.randomSplit([0.8, 0.2])
        
        als = ALS(maxIter=20, 
                  implicitPrefs=True,
                  userCol="user_id", 
                  itemCol="sale_id", 
                  ratingCol="count", 
                  coldStartStrategy="drop")
        
        paramGrid = ParamGridBuilder() \
            .addGrid(als.rank, [2, 5, 10, 20]) \
            .build()
        
        evaluator = RegressionEvaluator(metricName="rmse", labelCol="count", predictionCol="prediction")
        
        crossval = CrossValidator(estimator=als,
                                  estimatorParamMaps=paramGrid,
                                  evaluator=evaluator,
                                  numFolds=2)
        
        model = crossval.fit(self.training).bestModel
        #model = als.fit(self.training)
        self.model['sale_factors']['latent'] = np.array([row for row in model.itemFactors.toPandas().features])
        self.update_sale_mapping(model.itemFactors.toPandas())
        #self.model['user_factors'] = np.array([row for row in model.userFactors.toPandas().features])
        #self.update_user_mapping(model.userFactors.toPandas())
        self.users = pd.merge(
            pd.DataFrame.from_dict(self.model['user_mapping']['by_user'], 
                                   orient='index', 
                                   columns=['id']).reset_index(),
            model.userFactors.toPandas(),
            on='id',
            how='left'
        ).dropna(how='any').reset_index()[['index', 'features']]        
        
    def fetch_data(self):
        '''
        Retrieve users from the PostgreSQL database that have been mapped in the recommender model.

        :model: model saved as a dictionary with SparkRecommenderSystem
        :return: None
        '''
        conn = connect(
            host=self.host, 
            database=self.database, 
            user=self.user, 
            password=self.password
        )
        
        sql_query = 'SELECT user_key, apps FROM users WHERE apps IS NOT NULL AND user_key IN {}'
        
        data = pd.io.sql.read_sql_query(
            sql_query.format(str(tuple(self.users['index'].values))), 
            conn
        )
        result = self.users.rename(columns={"index": "user_key"}).merge(data, how='left', on='user_key')
        result.dropna(how='any', inplace=True)
        result = result.reset_index().drop('index', axis=1)
        
        self.model['user_mapping']['by_index'] = result['user_key'].to_dict()
        self.model['user_mapping']['by_user'] = invert_mapping(self.model['user_mapping']['by_index'])
        self.model['user_factors']['latent'] = np.array([row for row in result.features])
        
        self.apps = pd.io.json.json_normalize(result['apps'])
        self.model['app_mapping']['by_index'] = pd.DataFrame(self.apps.columns.values)[0].to_dict()
        self.model['app_mapping']['by_app'] = invert_mapping(self.model['app_mapping']['by_index'])
        self.__transform_user_factors()

    def __transform_user_factors(self):
        '''         
        Pipeline to transform user properties into numeric values. In this specific case
        the pipeline consists in: 
        
        - filling the missing values with the most frequent value
        for each app.
        - converting the pandas DF into a numpy array.  
        '''
        for app in self.apps.columns.values:
            self.apps[app].fillna(self.apps[app].mode()[0], inplace=True)
        self.model['user_factors']['apps'] = (self.apps * 1).values

    def upload_model(self):
        date = datetime.datetime.now().strftime("%Y-%m-%d")
        model_path = 's3a://{}/sales/specialnew/{}'.format(self.aws_bucket_models, date)

        print("Saving the Spark model into pickled files...")
        client = boto3.client(
            's3',
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key
        )

        client.put_object(
            Body=pickle.dumps(self.model), 
            Bucket=self.aws_bucket_models, 
            Key='sales/specialnew/{}/model.pickle'.format(date)
        )     

        client.put_object(
            Body=json.dumps({'version': date}), 
            Bucket=self.aws_bucket_models, 
            Key='sales/specialnew/version.json'.format(date)
        )   
        

