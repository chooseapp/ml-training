import argparse
import json

from ml_training.sales import model as recommender_model
from ml_training.gender import model as gender_model

CONFIG_PATH = '/etc/ml-training.json'

parser = argparse.ArgumentParser()
parser.add_argument("--config")
args = parser.parse_args()

with open(args.config or CONFIG_PATH, 'r') as f:
    config = json.load(f)


if __name__ == '__main__':
    reco = recommender_model.SparkRecommenderSystem(config=config)
    reco.train_model(condition="event_name IN  ('ClickItem', 'AddToCart') AND event_time > 1543669201")        
    reco.fetch_data()    
    reco.upload_model()
    gender = gender_model.GenderModel(config=config)
    gender.fit()
    gender.upload_model()

