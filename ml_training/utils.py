def invert_mapping(d: dict) -> dict:
    return {v: k for k, v in d.items()}