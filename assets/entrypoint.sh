#!/bin/sh
set -e

# Pull the ML training repository at the given REF, and start the training app.

if [ -z $1 ]
then
    echo "usage $0 GIT_REF [--post-result] [PYTHON_MODULE]"
    exit 1
fi

PYTHON_MODULE="ml_training.app"
REF=$1
shift

while [ $# -ne 0 ]
do
    if [ $1 = "--post-result" ]
    then
        INSTANCE=$(curl http://169.254.169.254/latest/meta-data/instance-id)
    else
        PYTHON_MODULE=$1
    fi
    shift
done

git clone -b ${REF} ${REPO} app
. /opt/venv/bin/activate
pip install -e app/
if [ -z $INSTANCE ]
then
    python -m ${PYTHON_MODULE}
else
    python -m ${PYTHON_MODULE} >out 2>&1
    LOG=$(python -c "import json; print(json.dumps(open('out').read()))")
    if [ $? -eq 0 ]
    then
        STATUS=COMPLETED
    else
        STATUS=FAILED
    fi
    curl -i -X POST -d "{\"status\": \"$STATUS\", \"log\": $LOG}" \
        -H 'Content-Type: application/json' \
        "${COORDINATOR_ENDPOINT}/${INSTANCE}"
fi
