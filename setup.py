import os

from setuptools import find_packages, setup

def dependencies():
    """
    Obtain the dependencies from requirements.txt.
    """
    with open('requirements.txt') as reqs:
        return reqs.read().splitlines()

setup(
    name='ml_training',
    version='0.0.1',
    description='AppChoose ML Training Service',
    author='AppChoose',
    packages=find_packages(exclude=['tests']),
    install_requires=dependencies(),
)
